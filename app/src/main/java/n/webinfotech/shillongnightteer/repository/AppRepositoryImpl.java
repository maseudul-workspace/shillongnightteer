package n.webinfotech.shillongnightteer.repository;

import android.util.Log;

import com.google.gson.Gson;

import n.webinfotech.shillongnightteer.domain.models.CalendarWrapper;
import n.webinfotech.shillongnightteer.domain.models.CommonNumberWrapper;
import n.webinfotech.shillongnightteer.domain.models.HomeDataWrapper;
import n.webinfotech.shillongnightteer.domain.models.PreviousResultsWrapper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 15-08-2019.
 */

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = ApiClient.createService(AppRepository.class);
    }

    public HomeDataWrapper fetchHomeDatas() {
        HomeDataWrapper homeDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchHomeData();

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    homeDataWrapper = null;
                }else{
                    homeDataWrapper = gson.fromJson(responseBody, HomeDataWrapper.class);
                }
            } else {
                homeDataWrapper = null;
            }
        }catch (Exception e){
            homeDataWrapper = null;
        }
        return homeDataWrapper;
    }

    public CommonNumberWrapper fetchCommonNumbers(int gameId) {
        CommonNumberWrapper commonNumberWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCommonNumber(gameId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonNumberWrapper = null;
                }else{
                    commonNumberWrapper = gson.fromJson(responseBody, CommonNumberWrapper.class);
                }
            } else {
                commonNumberWrapper = null;
            }
        }catch (Exception e){
            commonNumberWrapper = null;
        }
        return commonNumberWrapper;
    }

    public PreviousResultsWrapper fetchPreviousResults(int gameId, int pageNo) {
        PreviousResultsWrapper previousResultsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchPreviousResults(gameId, pageNo);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    previousResultsWrapper = null;
                }else{
                    previousResultsWrapper = gson.fromJson(responseBody, PreviousResultsWrapper.class);
                }
            } else {
                previousResultsWrapper = null;
            }
        }catch (Exception e){
            previousResultsWrapper = null;
        }
        return previousResultsWrapper;
    }

    public CalendarWrapper fetchCalendar(int pageNo) {
        CalendarWrapper calendarWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCalendar(pageNo);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    calendarWrapper = null;
                }else{
                    calendarWrapper = gson.fromJson(responseBody, CalendarWrapper.class);
                }
            } else {
                calendarWrapper = null;
            }
        }catch (Exception e){
            calendarWrapper = null;
        }
        return calendarWrapper;
    }

}
