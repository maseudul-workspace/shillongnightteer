package n.webinfotech.shillongnightteer.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Raj on 15-08-2019.
 */

public interface AppRepository {

    @GET("api/home_api.php")
    Call<ResponseBody> fetchHomeData();

    @POST("api/common_number.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchCommonNumber(@Field("gameSlotId") int gameSlotId);

    @POST("api/result.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchPreviousResults(@Field("gameId") int gameId,
                                            @Field("page_no") int pageNo
                                            );

    @POST("api/calender.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchCalendar(@Field("page_no") int pageNo);

}
