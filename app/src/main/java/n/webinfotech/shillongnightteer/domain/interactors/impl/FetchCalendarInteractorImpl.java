package n.webinfotech.shillongnightteer.domain.interactors.impl;

import n.webinfotech.shillongnightteer.domain.executors.Executor;
import n.webinfotech.shillongnightteer.domain.executors.MainThread;
import n.webinfotech.shillongnightteer.domain.interactors.FetchCalendarInteractor;
import n.webinfotech.shillongnightteer.domain.interactors.base.AbstractInteractor;
import n.webinfotech.shillongnightteer.domain.models.Calendar;
import n.webinfotech.shillongnightteer.domain.models.CalendarWrapper;
import n.webinfotech.shillongnightteer.repository.AppRepositoryImpl;

/**
 * Created by Raj on 16-08-2019.
 */

public class FetchCalendarInteractorImpl extends AbstractInteractor implements FetchCalendarInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int pageNo;

    public FetchCalendarInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int pageNo) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.pageNo = pageNo;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDataFail(errorMsg);
            }
        });
    }

    private void postMessage(final Calendar[] calendars, final int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDataSuccess(calendars, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final CalendarWrapper calendarWrapper = mRepository.fetchCalendar(pageNo);
        if(calendarWrapper == null) {
            notifyError("Something went wrong");
        } else if(!calendarWrapper.status) {
            notifyError("Something went wrong");
        } else{
            postMessage(calendarWrapper.calendars, calendarWrapper.total_page);
        }
    }
}
