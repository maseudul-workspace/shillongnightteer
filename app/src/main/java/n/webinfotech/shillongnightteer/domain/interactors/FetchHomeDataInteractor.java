package n.webinfotech.shillongnightteer.domain.interactors;

import n.webinfotech.shillongnightteer.domain.models.HomeData;

/**
 * Created by Raj on 15-08-2019.
 */

public interface FetchHomeDataInteractor {
    interface Callback {
        void onGettingDataSuccess(HomeData homeData);
        void onGettingDataFail(String errorMsg);
    }
}
