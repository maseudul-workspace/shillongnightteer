package n.webinfotech.shillongnightteer.domain.interactors;

import n.webinfotech.shillongnightteer.domain.models.CommonNumber;
import n.webinfotech.shillongnightteer.domain.models.CommonNumberWrapper;

/**
 * Created by Raj on 16-08-2019.
 */

public interface FetchCommonNumbersInteractor {
    interface Callback {
        void onGettingCommonNumberSuccess(CommonNumberWrapper commonNumberWrapper);
        void onGettingCommonNumberFail(String errorMsg);
    }
}
