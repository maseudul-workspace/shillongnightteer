package n.webinfotech.shillongnightteer.domain.interactors;

import n.webinfotech.shillongnightteer.domain.models.PreviousResults;

/**
 * Created by Raj on 16-08-2019.
 */

public interface FetchPreviousResultsInteractor {
    interface Callback {
        void onGettingDataSuccess(PreviousResults[] previousResults, int totalPage);
        void onGettingDataFail(String errorMsg);
    }
}
