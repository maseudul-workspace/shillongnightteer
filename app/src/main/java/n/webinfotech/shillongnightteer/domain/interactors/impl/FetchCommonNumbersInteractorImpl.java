package n.webinfotech.shillongnightteer.domain.interactors.impl;

import n.webinfotech.shillongnightteer.domain.executors.Executor;
import n.webinfotech.shillongnightteer.domain.executors.MainThread;
import n.webinfotech.shillongnightteer.domain.interactors.FetchCommonNumbersInteractor;
import n.webinfotech.shillongnightteer.domain.interactors.base.AbstractInteractor;
import n.webinfotech.shillongnightteer.domain.models.CommonNumber;
import n.webinfotech.shillongnightteer.domain.models.CommonNumberWrapper;
import n.webinfotech.shillongnightteer.repository.AppRepositoryImpl;

/**
 * Created by Raj on 16-08-2019.
 */

public class FetchCommonNumbersInteractorImpl extends AbstractInteractor implements FetchCommonNumbersInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int gameId;

    public FetchCommonNumbersInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int gameId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.gameId = gameId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCommonNumberFail(errorMsg);
            }
        });
    }

    private void postMessage(final CommonNumberWrapper commonNumberWrapper){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCommonNumberSuccess(commonNumberWrapper);
            }
        });
    }

    @Override
    public void run() {
        final CommonNumberWrapper commonNumberWrapper = mRepository.fetchCommonNumbers(gameId);
        if(commonNumberWrapper == null) {
            notifyError("Something went wrong");
        } else if(!commonNumberWrapper.status) {
            notifyError("Something went wrong");
        } else {
            postMessage(commonNumberWrapper);
        }
    }
}
