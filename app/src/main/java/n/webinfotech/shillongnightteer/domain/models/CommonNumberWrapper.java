package n.webinfotech.shillongnightteer.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 16-08-2019.
 */

public class CommonNumberWrapper {
    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("data")
    @Expose
    public CommonNumber[] commonNumbers;
}
