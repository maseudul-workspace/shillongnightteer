package n.webinfotech.shillongnightteer.domain.interactors.impl;

import android.content.Context;

import n.webinfotech.shillongnightteer.domain.executors.Executor;
import n.webinfotech.shillongnightteer.domain.executors.MainThread;
import n.webinfotech.shillongnightteer.domain.interactors.FetchCommonNumbersInteractor;
import n.webinfotech.shillongnightteer.domain.interactors.FetchPreviousResultsInteractor;
import n.webinfotech.shillongnightteer.domain.interactors.base.AbstractInteractor;
import n.webinfotech.shillongnightteer.domain.models.CommonNumber;
import n.webinfotech.shillongnightteer.domain.models.PreviousResults;
import n.webinfotech.shillongnightteer.domain.models.PreviousResultsWrapper;
import n.webinfotech.shillongnightteer.repository.AppRepositoryImpl;

/**
 * Created by Raj on 16-08-2019.
 */

public class FetchPreviousResultsInteractorImpl extends AbstractInteractor implements FetchPreviousResultsInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int gameId;
    int pageNo;

    public FetchPreviousResultsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int gameId, int pageNo) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.gameId = gameId;
        this.pageNo = pageNo;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDataFail(errorMsg);
            }
        });
    }

    private void postMessage(final PreviousResults[] previousResults, final int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDataSuccess(previousResults, totalPage);
            }
        });
    }


    @Override
    public void run() {
        final PreviousResultsWrapper previousResultsWrapper = mRepository.fetchPreviousResults(gameId, pageNo);
        if(previousResultsWrapper == null) {
            notifyError("Something went wrong");
        } else if(!previousResultsWrapper.status) {
            notifyError("Something went wrong");
        } else {
            postMessage(previousResultsWrapper.previousResults, previousResultsWrapper.total_page);
        }
    }
}
