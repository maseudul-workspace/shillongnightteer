package n.webinfotech.shillongnightteer.domain.interactors.impl;

import n.webinfotech.shillongnightteer.domain.executors.Executor;
import n.webinfotech.shillongnightteer.domain.executors.MainThread;
import n.webinfotech.shillongnightteer.domain.interactors.FetchHomeDataInteractor;
import n.webinfotech.shillongnightteer.domain.interactors.base.AbstractInteractor;
import n.webinfotech.shillongnightteer.domain.models.HomeData;
import n.webinfotech.shillongnightteer.domain.models.HomeDataWrapper;
import n.webinfotech.shillongnightteer.repository.AppRepository;
import n.webinfotech.shillongnightteer.repository.AppRepositoryImpl;

/**
 * Created by Raj on 15-08-2019.
 */

public class FetchHomeDataInteractorImpl extends AbstractInteractor implements FetchHomeDataInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;

    public FetchHomeDataInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDataFail(errorMsg);
            }
        });
    }

    private void postMessage(final HomeData homeData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDataSuccess(homeData);
            }
        });
    }

    @Override
    public void run() {
        final HomeDataWrapper homeDataWrapper = mRepository.fetchHomeDatas();
        if(homeDataWrapper == null) {
            notifyError("Something went wrong");
        } else if(!homeDataWrapper.status) {
            notifyError("Something went wrong");
        } else {
            postMessage(homeDataWrapper.homeData);
        }
    }

}
