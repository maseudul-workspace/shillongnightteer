package n.webinfotech.shillongnightteer.domain.interactors;

import n.webinfotech.shillongnightteer.domain.models.Calendar;

/**
 * Created by Raj on 16-08-2019.
 */

public interface FetchCalendarInteractor {
    interface Callback {
        void onGettingDataSuccess(Calendar[] calendars, int totalPage);
        void onGettingDataFail(String errorMsg);
    }
}
