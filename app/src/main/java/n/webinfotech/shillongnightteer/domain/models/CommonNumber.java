package n.webinfotech.shillongnightteer.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 16-08-2019.
 */

public class CommonNumber {

    @SerializedName("direct")
    @Expose
    public String direct;

    @SerializedName("house")
    @Expose
    public String house;

    @SerializedName("ending")
    @Expose
    public String ending;

}
