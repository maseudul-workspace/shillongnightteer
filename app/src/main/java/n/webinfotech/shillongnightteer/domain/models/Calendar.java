package n.webinfotech.shillongnightteer.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 16-08-2019.
 */

public class Calendar {

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("day")
    @Expose
    public String day;

    @SerializedName("options")
    @Expose
    public String status;

}
