package n.webinfotech.shillongnightteer.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 15-08-2019.
 */

public class Shillong {

    @SerializedName("FRSCORE")
    @Expose
    public String firstRoundScore;

    @SerializedName("FRTIME")
    @Expose
    public String firstRoundTime;

    @SerializedName("SRSCORE")
    @Expose
    public String secondRoundScore;

    @SerializedName("SRTIME")
    @Expose
    public String secondRoundTime;

    @SerializedName("DATE")
    @Expose
    public String date;

}
