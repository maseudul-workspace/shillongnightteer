package n.webinfotech.shillongnightteer.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 15-08-2019.
 */

public class HomeData {

    @SerializedName("shillong_night_teer")
    @Expose
    public ShillongNightTeer shillongNightTeer;

    @SerializedName("shillong")
    @Expose
    public Shillong shillong;

    @SerializedName("khanapara")
    @Expose
    public Khanapara khanapara;

}
