package n.webinfotech.shillongnightteer.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 16-08-2019.
 */

public class CalendarWrapper {
    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("total_page")
    @Expose
    public int total_page;

    @SerializedName("data")
    @Expose
    public Calendar[] calendars;
}
