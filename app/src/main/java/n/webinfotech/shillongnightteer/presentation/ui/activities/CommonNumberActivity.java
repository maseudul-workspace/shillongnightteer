package n.webinfotech.shillongnightteer.presentation.ui.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.shillongnightteer.R;
import n.webinfotech.shillongnightteer.domain.executors.impl.ThreadExecutor;
import n.webinfotech.shillongnightteer.presentation.presenters.CommonNumberPresenter;
import n.webinfotech.shillongnightteer.presentation.presenters.impl.CommonNumberPresenterImpl;
import n.webinfotech.shillongnightteer.presentation.ui.adapters.CommonNumberAdapter;
import n.webinfotech.shillongnightteer.threading.MainThreadImpl;

public class CommonNumberActivity extends BaseActivity implements CommonNumberPresenter.View {

    @BindView(R.id.recycler_view_common_numbers)
    RecyclerView recyclerViewCommonNumbers;
    @BindView(R.id.layout_loader)
    View layoutLoader;
    @BindView(R.id.txt_view_game_name)
    TextView txtViewGameName;
    @BindView(R.id.txt_view_game_date)
    TextView txtViewGameDate;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    CommonNumberPresenterImpl mPresenter;
    int gameId;
    String gameName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_common_number);
        ButterKnife.bind(this);
        initialisePresenter();
        setSwipeRefreshLayout();
        gameId = getIntent().getIntExtra("gameId", 0);
        gameName = getIntent().getStringExtra("gameName");
        txtViewGameName.setText(gameName);
        mPresenter.fetchCommonNumbers(gameId);
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new CommonNumberPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setSwipeRefreshLayout(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.fetchCommonNumbers(gameId);
            }
        });
    }

    @Override
    public void loadData(CommonNumberAdapter adapter, String date) {
        txtViewGameDate.setText(date);
        recyclerViewCommonNumbers.setVisibility(View.VISIBLE);
        recyclerViewCommonNumbers.setAdapter(adapter);
        recyclerViewCommonNumbers.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public void stopRefreshing() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
        {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
