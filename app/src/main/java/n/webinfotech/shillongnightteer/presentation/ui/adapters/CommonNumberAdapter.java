package n.webinfotech.shillongnightteer.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.shillongnightteer.R;
import n.webinfotech.shillongnightteer.domain.models.CommonNumber;

/**
 * Created by Raj on 08-08-2019.
 */

public class CommonNumberAdapter extends RecyclerView.Adapter<CommonNumberAdapter.ViewHolder> {

    Context mContext;
    CommonNumber[] commonNumbers;

    public CommonNumberAdapter(Context mContext, CommonNumber[] commonNumbers) {
        this.mContext = mContext;
        this.commonNumbers = commonNumbers;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_common_number, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtViewDirect.setText(commonNumbers[i].direct);
        viewHolder.txtViewHouse.setText(commonNumbers[i].house);
        viewHolder.txtViewEnding.setText(commonNumbers[i].ending);
    }

    @Override
    public int getItemCount() {
        return commonNumbers.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_direct)
        TextView txtViewDirect;
        @BindView(R.id.txt_view_house)
        TextView txtViewHouse;
        @BindView(R.id.txt_view_ending)
        TextView txtViewEnding;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
