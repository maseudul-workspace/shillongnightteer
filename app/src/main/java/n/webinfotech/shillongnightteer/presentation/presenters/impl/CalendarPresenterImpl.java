package n.webinfotech.shillongnightteer.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import n.webinfotech.shillongnightteer.domain.executors.Executor;
import n.webinfotech.shillongnightteer.domain.executors.MainThread;
import n.webinfotech.shillongnightteer.domain.interactors.FetchCalendarInteractor;
import n.webinfotech.shillongnightteer.domain.interactors.impl.FetchCalendarInteractorImpl;
import n.webinfotech.shillongnightteer.domain.models.Calendar;
import n.webinfotech.shillongnightteer.presentation.presenters.CalendarPresenter;
import n.webinfotech.shillongnightteer.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.shillongnightteer.presentation.ui.adapters.CalendarAdapter;
import n.webinfotech.shillongnightteer.repository.AppRepositoryImpl;

/**
 * Created by Raj on 16-08-2019.
 */

public class CalendarPresenterImpl extends AbstractPresenter implements CalendarPresenter, FetchCalendarInteractor.Callback {

    Context mContext;
    CalendarPresenter.View mView;
    Calendar[] newCalendars;
    FetchCalendarInteractorImpl mInteractor;
    CalendarAdapter adapter;

    public CalendarPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchData(int pageNo, String type) {
        if(type.equals("refresh")) {
            newCalendars = null;
        }
        mInteractor = new FetchCalendarInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), pageNo);
        mInteractor.execute();
    }

    @Override
    public void onGettingDataSuccess(Calendar[] calendars, int totalPage) {
        Calendar[] tempCalendars;
        tempCalendars = newCalendars;
        try {
            int len1 = tempCalendars.length;
            int len2 = calendars.length;
            newCalendars = new Calendar[len1 + len2];
            System.arraycopy(tempCalendars, 0, newCalendars, 0, len1);
            System.arraycopy(calendars, 0, newCalendars, len1, len2);
            adapter.updateData(newCalendars);
            adapter.notifyDataSetChanged();
            mView.hidePaginationLoader();
        } catch (NullPointerException e) {
            mView.hideLoader();
            newCalendars = calendars;
            adapter = new CalendarAdapter(mContext, calendars);
            mView.loadData(adapter, totalPage);
        }
        mView.stopRefreshing();
    }

    @Override
    public void onGettingDataFail(String errorMsg) {
        mView.hideLoader();
        mView.hidePaginationLoader();
        mView.stopRefreshing();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
