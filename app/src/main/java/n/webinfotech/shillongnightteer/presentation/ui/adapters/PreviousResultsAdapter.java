package n.webinfotech.shillongnightteer.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.shillongnightteer.R;
import n.webinfotech.shillongnightteer.domain.models.PreviousResults;

/**
 * Created by Raj on 08-08-2019.
 */

public class PreviousResultsAdapter extends RecyclerView.Adapter<PreviousResultsAdapter.ViewHolder> {

    Context mContext;
    PreviousResults[] previousResults;

    public PreviousResultsAdapter(Context mContext, PreviousResults[] previousResults) {
        this.mContext = mContext;
        this.previousResults = previousResults;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_previous_results, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtViewDate.setText(previousResults[i].date);
        viewHolder.txtViewScoreFirstRound.setText(previousResults[i].firstRoundScore);
        viewHolder.txtViewScoreSecondRound.setText(previousResults[i].secondRoundScore);
        viewHolder.txtViewTimeFirstRound.setText(previousResults[i].firstRoundTime);
        viewHolder.txtViewTimeSecondRound.setText(previousResults[i].secondRoundTime);
    }

    @Override
    public int getItemCount() {
        return previousResults.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_score_first_round)
        TextView txtViewScoreFirstRound;
        @BindView(R.id.txt_view_score_second_round)
        TextView txtViewScoreSecondRound;
        @BindView(R.id.txt_view_time_first_round)
        TextView txtViewTimeFirstRound;
        @BindView(R.id.txt_view_second_round_time)
        TextView txtViewTimeSecondRound;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(PreviousResults[] previousResults) {
        this.previousResults = previousResults;
    }

}
