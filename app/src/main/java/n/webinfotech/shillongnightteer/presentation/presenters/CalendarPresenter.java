package n.webinfotech.shillongnightteer.presentation.presenters;

import n.webinfotech.shillongnightteer.presentation.ui.adapters.CalendarAdapter;

/**
 * Created by Raj on 16-08-2019.
 */

public interface CalendarPresenter {

    void fetchData(int pageNo, String type);

    interface View {
        void loadData(CalendarAdapter adapter, int totalPage);
        void showLoader();
        void hideLoader();
        void showPaginationLoader();
        void hidePaginationLoader();
        void stopRefreshing();
    }
}
