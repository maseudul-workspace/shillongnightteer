package n.webinfotech.shillongnightteer.presentation.ui.activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.shillongnightteer.R;
import n.webinfotech.shillongnightteer.domain.executors.impl.ThreadExecutor;
import n.webinfotech.shillongnightteer.domain.models.HomeData;
import n.webinfotech.shillongnightteer.presentation.presenters.MainPresenter;
import n.webinfotech.shillongnightteer.presentation.presenters.impl.MainPresenterImpl;
import n.webinfotech.shillongnightteer.threading.MainThreadImpl;

public class MainActivity extends BaseActivity implements MainPresenter.View {

    @BindView(R.id.txt_view_shillong_night_date)
    TextView txtViewShillongNightDate;
    @BindView(R.id.txt_view_shillong_night_first_round_score)
    TextView txtViewShillongNightFirstRoundScore;
    @BindView(R.id.txt_view_shillong_night_first_round_time)
    TextView txtViewShillongNightFirstRoundTime;
    @BindView(R.id.txt_view_shillong_night_second_round_time)
    TextView txtViewShillongNightSecondRoundTime;
    @BindView(R.id.txt_view_shillong_night_second_round_score)
    TextView txtViewShillongNightSecondRoundScore;
    @BindView(R.id.txt_view_shillong_date)
    TextView txtViewShillongDate;
    @BindView(R.id.txt_view_shillong_first_round_time)
    TextView txtViewShillongFirstRoundTime;
    @BindView(R.id.txt_view_shillong_first_round_score)
    TextView txtViewShillongFirstRoundScore;
    @BindView(R.id.txt_view_shillong_second_round_time)
    TextView txtViewShillongSecondRoundTime;
    @BindView(R.id.txt_view_shillong_second_round_score)
    TextView txtViewShillongSecondRoundScore;
    @BindView(R.id.txt_view_khanapara_date)
    TextView txtViewKhanaparaDate;
    @BindView(R.id.txt_view_khanapara_first_round_score)
    TextView txtViewKhanaparaFirstRoundScore;
    @BindView(R.id.txt_view_khanapara_first_round_time)
    TextView txtViewKhanaparaFirstRoundTime;
    @BindView(R.id.txt_view_khanapara_second_round_score)
    TextView txtViewKhanaparaSecondRoundScore;
    @BindView(R.id.txt_view_khanapara_second_round_time)
    TextView txtViewKhanaparaSecondRoundTime;
    @BindView(R.id.main_layout)
    View layoutMain;
    @BindView(R.id.layout_loader)
    View layoutLoader;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    MainPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        ButterKnife.bind(this);
        initialisePresenter();
        setSwipeRefreshLayout();
        mPresenter.fetchData();
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setSwipeRefreshLayout(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.fetchData();
            }
        });
    }

    @OnClick(R.id.layout_shillong_night_teer) void onShillongNightTeerClicked() {
        Intent shillongNightTeerpreviousActivityIntent = new Intent(getApplicationContext(), PreviousResultsActivity.class);
        shillongNightTeerpreviousActivityIntent.putExtra("gameId", 1);
        shillongNightTeerpreviousActivityIntent.putExtra("gameName", "Shillong Night Teer");
        startActivity(shillongNightTeerpreviousActivityIntent);
    }

    @OnClick(R.id.layout_shillong_night) void onShillongNightCLicked() {
        Intent shillongNightPreviousActivityIntent = new Intent(getApplicationContext(), PreviousResultsActivity.class);
        shillongNightPreviousActivityIntent.putExtra("gameId", 2);
        shillongNightPreviousActivityIntent.putExtra("gameName", "Shillong");
        startActivity(shillongNightPreviousActivityIntent);
    }

    @OnClick(R.id.layout_khanapara) void onKhanaparaClicked() {
        Intent khanaparaPreviousActivityIntent = new Intent(getApplicationContext(), PreviousResultsActivity.class);
        khanaparaPreviousActivityIntent.putExtra("gameId", 3);
        khanaparaPreviousActivityIntent.putExtra("gameName", "Khanapara");
        startActivity(khanaparaPreviousActivityIntent);
    }

    @Override
    public void loadData(HomeData homeData) {
        layoutMain.setVisibility(View.VISIBLE);
//        Shillong night teer
        txtViewShillongNightDate.setText(homeData.shillongNightTeer.date);
        txtViewShillongNightFirstRoundScore.setText(homeData.shillongNightTeer.firstRoundScore);
        txtViewShillongNightFirstRoundTime.setText(homeData.shillongNightTeer.firstRoundTime);
        txtViewShillongNightSecondRoundScore.setText(homeData.shillongNightTeer.secondRoundScore);
        txtViewShillongNightSecondRoundTime.setText(homeData.shillongNightTeer.secondRoundTime);

//        Shillong
        txtViewShillongDate.setText(homeData.shillong.date);
        txtViewShillongFirstRoundScore.setText(homeData.shillong.firstRoundScore);
        txtViewShillongFirstRoundTime.setText(homeData.shillong.firstRoundTime);
        txtViewShillongSecondRoundScore.setText(homeData.shillong.secondRoundScore);
        txtViewShillongSecondRoundTime.setText(homeData.shillong.secondRoundTime);

//        Khanapara
        txtViewKhanaparaDate.setText(homeData.khanapara.date);
        txtViewKhanaparaFirstRoundScore.setText(homeData.khanapara.firstRoundScore);
        txtViewKhanaparaFirstRoundTime.setText(homeData.khanapara.firstRoundTime);
        txtViewKhanaparaSecondRoundScore.setText(homeData.khanapara.secondRoundScore);
        txtViewKhanaparaSecondRoundTime.setText(homeData.khanapara.secondRoundTime);

    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public void stopRefreshing() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
        {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
