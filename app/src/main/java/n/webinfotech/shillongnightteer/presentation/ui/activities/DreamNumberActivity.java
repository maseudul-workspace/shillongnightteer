package n.webinfotech.shillongnightteer.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import n.webinfotech.shillongnightteer.R;

public class DreamNumberActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_dream_number);
    }

}
