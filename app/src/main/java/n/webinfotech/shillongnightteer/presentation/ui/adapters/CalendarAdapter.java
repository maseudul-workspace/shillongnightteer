package n.webinfotech.shillongnightteer.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.shillongnightteer.R;
import n.webinfotech.shillongnightteer.domain.models.Calendar;

/**
 * Created by Raj on 16-08-2019.
 */

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.ViewHolder> {

    Context mContext;
    Calendar[] calendars;

    public CalendarAdapter(Context mContext, Calendar[] calendars) {
        this.mContext = mContext;
        this.calendars = calendars;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_calendar, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtViewCalendarDate.setText(calendars[i].date);
        viewHolder.txtViewCalendarDay.setText(calendars[i].day);
        viewHolder.txtViewCalendarStatus.setText(calendars[i].status);
        if(calendars[i].status.equals("Yes")) {
            viewHolder.txtViewCalendarStatus.setTextColor(mContext.getResources().getColor(R.color.green2));
        } else {
            viewHolder.txtViewCalendarStatus.setTextColor(mContext.getResources().getColor(R.color.red2));

        }
    }

    @Override
    public int getItemCount() {
        return calendars.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_calendar_date)
        TextView txtViewCalendarDate;
        @BindView(R.id.txt_view_calendar_day)
        TextView txtViewCalendarDay;
        @BindView(R.id.txt_view_calendar_status)
        TextView txtViewCalendarStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(Calendar[] calendars) {
        this.calendars = calendars;
    }

}
