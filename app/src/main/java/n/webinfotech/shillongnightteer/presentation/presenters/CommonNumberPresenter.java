package n.webinfotech.shillongnightteer.presentation.presenters;

import n.webinfotech.shillongnightteer.presentation.ui.adapters.CommonNumberAdapter;

/**
 * Created by Raj on 16-08-2019.
 */

public interface CommonNumberPresenter {
    void fetchCommonNumbers(int gameId);
    interface View {
        void loadData(CommonNumberAdapter adapter, String date);
        void showLoader();
        void hideLoader();
        void stopRefreshing();
    }
}
