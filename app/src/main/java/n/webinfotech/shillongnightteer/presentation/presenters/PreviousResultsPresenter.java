package n.webinfotech.shillongnightteer.presentation.presenters;

import n.webinfotech.shillongnightteer.presentation.ui.adapters.PreviousResultsAdapter;

/**
 * Created by Raj on 16-08-2019.
 */

public interface PreviousResultsPresenter {
    void fetchPreviousResults(int gameId, int totalPage, String type);
    interface View {
        void loadAdapter(PreviousResultsAdapter adapter, int pageNo);
        void showLoader();
        void hideLoader();
        void showPaginationLoader();
        void hidePaginationLoader();
        void stopRefreshing();
    }
}
