package n.webinfotech.shillongnightteer.presentation.ui.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AbsListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.shillongnightteer.R;
import n.webinfotech.shillongnightteer.domain.executors.impl.ThreadExecutor;
import n.webinfotech.shillongnightteer.presentation.presenters.CalendarPresenter;
import n.webinfotech.shillongnightteer.presentation.presenters.impl.CalendarPresenterImpl;
import n.webinfotech.shillongnightteer.presentation.ui.adapters.CalendarAdapter;
import n.webinfotech.shillongnightteer.threading.MainThreadImpl;

public class CalendarActivity extends BaseActivity implements CalendarPresenter.View {

    @BindView(R.id.recycler_view_calendar)
    RecyclerView recyclerViewCalendar;
    @BindView(R.id.layout_loader)
    View layoutLoader;
    @BindView(R.id.pagination_loader)
    View paginationLoader;
    CalendarPresenterImpl mPresenter;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int totalPage = 1;
    int pageNo = 1;
    LinearLayoutManager layoutManager;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_calendar);
        ButterKnife.bind(this);
        initialisePresenter();
        setSwipeRefreshLayout();
        showLoader();
        mPresenter.fetchData(pageNo, "refresh");
    }

    public void initialisePresenter() {
        mPresenter = new CalendarPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setSwipeRefreshLayout(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                totalPage = 1;
                pageNo = 1;
                isScrolling = false;
                mPresenter.fetchData(pageNo, "refresh");
            }
        });
    }

    @Override
    public void loadData(CalendarAdapter adapter, final int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewCalendar.setVisibility(View.VISIBLE);
        recyclerViewCalendar.setAdapter(adapter);
        recyclerViewCalendar.setLayoutManager(layoutManager);
        recyclerViewCalendar.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        showPaginationLoader();
                        mPresenter.fetchData(pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public void showPaginationLoader() {
        paginationLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePaginationLoader() {
        paginationLoader.setVisibility(View.GONE);
    }

    @Override
    public void stopRefreshing() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
        {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
