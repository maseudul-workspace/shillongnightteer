package n.webinfotech.shillongnightteer.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import n.webinfotech.shillongnightteer.domain.executors.Executor;
import n.webinfotech.shillongnightteer.domain.executors.MainThread;
import n.webinfotech.shillongnightteer.domain.interactors.FetchPreviousResultsInteractor;
import n.webinfotech.shillongnightteer.domain.interactors.impl.FetchPreviousResultsInteractorImpl;
import n.webinfotech.shillongnightteer.domain.models.PreviousResults;
import n.webinfotech.shillongnightteer.presentation.presenters.PreviousResultsPresenter;
import n.webinfotech.shillongnightteer.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.shillongnightteer.presentation.ui.adapters.PreviousResultsAdapter;
import n.webinfotech.shillongnightteer.repository.AppRepositoryImpl;

/**
 * Created by Raj on 16-08-2019.
 */

public class PreviousResultsPresenterImpl extends AbstractPresenter implements PreviousResultsPresenter, FetchPreviousResultsInteractor.Callback {

    Context mContext;
    PreviousResultsPresenter.View mView;
    FetchPreviousResultsInteractorImpl mInteractor;
    PreviousResultsAdapter adapter;
    PreviousResults[] newResults;

    public PreviousResultsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchPreviousResults(int gameId, int pageNo, String type) {
        if(type.equals("refresh")){
            newResults = null;
        }
        mInteractor = new FetchPreviousResultsInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), gameId, pageNo);
        mInteractor.execute();
    }

    @Override
    public void onGettingDataSuccess(PreviousResults[] previousResults, int totalPage) {
        PreviousResults[] tempResults;
        tempResults = newResults;
        try {
            int len1 = tempResults.length;
            int len2 = previousResults.length;
            newResults = new PreviousResults[len1 + len2];
            System.arraycopy(tempResults, 0, newResults, 0, len1);
            System.arraycopy(previousResults, 0, newResults, len1, len2);
            adapter.updateDataSet(newResults);
            adapter.notifyDataSetChanged();
            mView.hidePaginationLoader();
        } catch (NullPointerException e) {
            mView.hideLoader();
            newResults = previousResults;
            adapter = new PreviousResultsAdapter(mContext, previousResults);
            mView.loadAdapter(adapter, totalPage);
        }
        mView.stopRefreshing();
    }

    @Override
    public void onGettingDataFail(String errorMsg) {
        mView.hideLoader();
        mView.hidePaginationLoader();
        mView.stopRefreshing();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();

    }
}
