package n.webinfotech.shillongnightteer.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import n.webinfotech.shillongnightteer.domain.executors.Executor;
import n.webinfotech.shillongnightteer.domain.executors.MainThread;
import n.webinfotech.shillongnightteer.domain.interactors.FetchHomeDataInteractor;
import n.webinfotech.shillongnightteer.domain.interactors.impl.FetchHomeDataInteractorImpl;
import n.webinfotech.shillongnightteer.domain.models.HomeData;
import n.webinfotech.shillongnightteer.presentation.presenters.MainPresenter;
import n.webinfotech.shillongnightteer.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.shillongnightteer.repository.AppRepositoryImpl;

/**
 * Created by Raj on 15-08-2019.
 */

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter, FetchHomeDataInteractor.Callback {

    Context mContext;
    MainPresenter.View mView;
    FetchHomeDataInteractorImpl mInteractor;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchData() {
        mInteractor = new FetchHomeDataInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        mInteractor.execute();
    }

    @Override
    public void onGettingDataSuccess(HomeData homeData) {
       mView.loadData(homeData);
       mView.hideLoader();
       mView.stopRefreshing();
    }

    @Override
    public void onGettingDataFail(String errorMsg) {
        mView.hideLoader();
        mView.stopRefreshing();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();

    }
}
