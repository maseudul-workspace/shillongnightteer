package n.webinfotech.shillongnightteer.presentation.ui.activities;

import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.shillongnightteer.R;
import n.webinfotech.shillongnightteer.domain.models.Calendar;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;
    Drawer drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    protected void inflateContent(@LayoutRes int inflatedResID) {
        setContentView(R.layout.activity_base);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflatedResID, contentFrameLayout);
        ButterKnife.bind(this);
        setDrawer();
    }

    public void setDrawer() {
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.primary_dark_gradient)
                .addProfiles(new ProfileDrawerItem().withIcon(R.drawable.grey_logo).withName("Shillong Night Teer"))
                .build();

        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .withHasStableIds(true)
                .withDisplayBelowStatusBar(false)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Home").withIdentifier(1).withIcon(R.drawable.home_red).withSelectable(true),
                        new ExpandableDrawerItem().withName("Shillong Night Teer").withIcon(R.drawable.location_red).withSelectable(true)
                                .withSubItems(
                                        new SecondaryDrawerItem().withName("Previous Results").withIcon(R.drawable.clock_red).withLevel(2).withIdentifier(2).withSelectable(false),
                                        new SecondaryDrawerItem().withName("Common Number").withIcon(R.drawable.dice_red).withLevel(2).withIdentifier(3).withSelectable(false)
                                        ),
                        new ExpandableDrawerItem().withName("Shillong").withIcon(R.drawable.location_red).withSelectable(false)
                                .withSubItems(
                                        new SecondaryDrawerItem().withName("Previous Results").withIcon(R.drawable.clock_red).withLevel(2).withIdentifier(4).withSelectable(false),
                                        new SecondaryDrawerItem().withName("Common Number").withIcon(R.drawable.dice_red).withLevel(2).withIdentifier(5).withSelectable(false)
                                                ),
                        new ExpandableDrawerItem().withName("Khanapara").withIcon(R.drawable.location_red).withSelectable(false)
                        .withSubItems(
                                new SecondaryDrawerItem().withName("Previous Results").withIcon(R.drawable.clock_red).withLevel(2).withIdentifier(6).withSelectable(false),
                                new SecondaryDrawerItem().withName("Common Number").withIcon(R.drawable.dice_red).withLevel(2).withIdentifier(7).withSelectable(false)
                                                ),
                        new PrimaryDrawerItem().withName("Dream Number").withIcon(R.drawable.dream_red).withSelectable(false).withIdentifier(8).withSelectable(false),
                        new PrimaryDrawerItem().withName("Teer Calendar").withIcon(R.drawable.calendar_red).withSelectable(false).withIdentifier(9).withSelectable(false)
                        ).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                            @Override
                            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                // do something with the clicked item :D
                                if (drawerItem != null) {
                                    switch (((int) drawerItem.getIdentifier())) {
                                        case 1:
                                            Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
                                            startActivity(mainActivityIntent);
                                            break;
                                        case 2:
                                            Intent shillongNightTeerpreviousActivityIntent = new Intent(getApplicationContext(), PreviousResultsActivity.class);
                                            shillongNightTeerpreviousActivityIntent.putExtra("gameId", 1);
                                            shillongNightTeerpreviousActivityIntent.putExtra("gameName", "Shillong Night Teer");
                                            startActivity(shillongNightTeerpreviousActivityIntent);
                                            break;
                                        case 3:
                                            Intent shillongNightTeerCommonNumberIntent = new Intent(getApplicationContext(), CommonNumberActivity.class);
                                            shillongNightTeerCommonNumberIntent.putExtra("gameId", 1);
                                            shillongNightTeerCommonNumberIntent.putExtra("gameName", "Shillong Night Teer");
                                            startActivity(shillongNightTeerCommonNumberIntent);
                                            break;
                                        case 4:
                                            Intent shillongNightPreviousActivityIntent = new Intent(getApplicationContext(), PreviousResultsActivity.class);
                                            shillongNightPreviousActivityIntent.putExtra("gameId", 2);
                                            shillongNightPreviousActivityIntent.putExtra("gameName", "Shillong");
                                            startActivity(shillongNightPreviousActivityIntent);
                                            break;
                                        case 5:
                                            Intent shillongNightCommonNumberIntent = new Intent(getApplicationContext(), CommonNumberActivity.class);
                                            shillongNightCommonNumberIntent.putExtra("gameId", 2);
                                            shillongNightCommonNumberIntent.putExtra("gameName", "Shillong");
                                            startActivity(shillongNightCommonNumberIntent);
                                            break;
                                        case 6:
                                            Intent khanaparaPreviousActivityIntent = new Intent(getApplicationContext(), PreviousResultsActivity.class);
                                            khanaparaPreviousActivityIntent.putExtra("gameId", 3);
                                            khanaparaPreviousActivityIntent.putExtra("gameName", "Khanapara");
                                            startActivity(khanaparaPreviousActivityIntent);
                                            break;
                                        case 7:
                                            Intent khanaparaCommonNumberIntent = new Intent(getApplicationContext(), CommonNumberActivity.class);
                                            khanaparaCommonNumberIntent.putExtra("gameId", 3);
                                            khanaparaCommonNumberIntent.putExtra("gameName", "Khanapara");
                                            startActivity(khanaparaCommonNumberIntent);
                                            break;
                                        case 8:
                                            Intent dreamNumberActivity = new Intent(getApplicationContext(), DreamNumberActivity.class);
                                            startActivity(dreamNumberActivity);
                                            break;
                                        case 9:
                                            Intent calendarIntent = new Intent(getApplicationContext(), CalendarActivity.class);
                                            startActivity(calendarIntent);
                                            break;
                                    }
                                }
                                return false;
                            }
                        })
                        .build();
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

    }

}
