package n.webinfotech.shillongnightteer.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import n.webinfotech.shillongnightteer.domain.executors.Executor;
import n.webinfotech.shillongnightteer.domain.executors.MainThread;
import n.webinfotech.shillongnightteer.domain.interactors.FetchCommonNumbersInteractor;
import n.webinfotech.shillongnightteer.domain.interactors.impl.FetchCommonNumbersInteractorImpl;
import n.webinfotech.shillongnightteer.domain.models.CommonNumber;
import n.webinfotech.shillongnightteer.domain.models.CommonNumberWrapper;
import n.webinfotech.shillongnightteer.presentation.presenters.CommonNumberPresenter;
import n.webinfotech.shillongnightteer.presentation.presenters.base.AbstractPresenter;
import n.webinfotech.shillongnightteer.presentation.ui.adapters.CommonNumberAdapter;
import n.webinfotech.shillongnightteer.repository.AppRepositoryImpl;

/**
 * Created by Raj on 16-08-2019.
 */

public class CommonNumberPresenterImpl extends AbstractPresenter implements CommonNumberPresenter, FetchCommonNumbersInteractor.Callback {

    Context mContext;
    CommonNumberPresenter.View mView;
    FetchCommonNumbersInteractorImpl mInteractor;

    public CommonNumberPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchCommonNumbers(int gameId) {
        mInteractor = new FetchCommonNumbersInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), gameId);
        mInteractor.execute();
    }

    @Override
    public void onGettingCommonNumberSuccess(CommonNumberWrapper commonNumberWrapper) {
        mView.hideLoader();
        mView.stopRefreshing();
        CommonNumberAdapter adapter = new CommonNumberAdapter(mContext, commonNumberWrapper.commonNumbers);
        mView.loadData(adapter, commonNumberWrapper.date);
    }

    @Override
    public void onGettingCommonNumberFail(String errorMsg) {
        mView.hideLoader();
        mView.stopRefreshing();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
