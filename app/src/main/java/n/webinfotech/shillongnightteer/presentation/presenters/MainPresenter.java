package n.webinfotech.shillongnightteer.presentation.presenters;

import n.webinfotech.shillongnightteer.domain.models.HomeData;

/**
 * Created by Raj on 15-08-2019.
 */

public interface MainPresenter {
    void fetchData();
    interface View {
        void loadData(HomeData homeData);
        void showLoader();
        void hideLoader();
        void stopRefreshing();
    }
}
