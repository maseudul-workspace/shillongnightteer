package n.webinfotech.shillongnightteer.presentation.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.shillongnightteer.R;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.img_view_arrow)
    ImageView imgViewArrow;
    @BindView(R.id.img_view_target)
    ImageView imgViewTarget;
    @BindView(R.id.layout_arrow_title)
    View layoutArrow;
    Animation fromBottom;
    Animation fromTop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        setAnimation();
    }

    public void setAnimation() {
        fromBottom = AnimationUtils.loadAnimation(this, R.anim.frombottom);
        fromTop = AnimationUtils.loadAnimation(this, R.anim.fromtop);
        imgViewTarget.setAnimation(fromTop);
        layoutArrow.setAnimation(fromBottom);
    }

    @OnClick(R.id.btn_get_started) void onBtnGetStartedClicked() {
        Intent mainIntent = new Intent(this, MainActivity.class);
        startActivity(mainIntent);
    }

}
